const parse = require('url-parse');
const sharp = require('sharp');
const crypto = require("crypto");
const fs = require('fs').promises;
const toml = require('toml');
const b58 = require('b58');
const fetch = require("node-fetch");
const https = require('https');
const StellarSdk = require('stellar-sdk');
const path = require('path');
const tqdm = require('tqdm');
const server = new StellarSdk.Server('https://dex.digitalworld.global');
var isRunning = false;
var totalAssets = 0;

const httpsAgent = new https.Agent({
  rejectUnauthorized: false,
});

// Helper function to ensure directory existence
const ensureDirectoryExists = async (directoryPath) => {
  try {
    await fs.access(directoryPath);
  } catch (error) {
    if (error.code === 'ENOENT') {
      await fs.mkdir(directoryPath);
    } else {
      throw error;
    }
  }
};

/* 
  Gets a list of the assets currently available on Stellar.
  
    Returns:
        array: of unique URLs pointing to the `stellar.toml` files that describe the assets.
*/
const getAssetList = async (testnet = false) => {
  var links = [];
  var completed = [];
  var cursor = "";

  var pages = 0;
  var limit = 200;
  while (cursor !== null && !completed.includes(cursor)) {
    completed.push(cursor);
    try {
      const response = await server.assets().cursor(cursor).limit(limit).call();
      response.records.forEach(asset => {
        var link = asset["_links"]["toml"]["href"];

        // Check if the link is for DALA which we need to fix manually.
        if (link != null && link == "https://DALA.digitalworld.management/.well-known/stellar.toml") {
          link = "https://DALA.tomlhawaiian.life/.well-known/stellar.toml"
        }

        if (link !== "") {
          links.push(link);
        }
        cursor = asset["paging_token"];
      });
      pages += 1;
    } catch (error) {
      console.error(error);
      return null;
    }
  }
  console.log(`Queried ${pages} pages with a limit of ${limit} assets per page.`);

  return links;
};
const fetchFile = async (url, target) => {
  try {
    const response = await fetch(url, { agent: httpsAgent });
    if (response.ok) {
      const data = await response.buffer();
      await fs.writeFile(target, data);
      // console.log(`File fetched and saved successfully: ${target}`);
    } else {
      // console.error(`Failed to fetch file from ${url}. Status: ${response.status}`);
    }
  } catch (error) {
    console.error(`An error occurred while fetching file from ${url}:`, error);
  }
};

const fetchIcon = async (url, target) => {
  try {
    const response = await fetch(url, { agent: httpsAgent });
    if (response.ok) {
      const data = await response.buffer();
      const resizedImage = await sharp(data)
        .resize({ width: 200 }) // Set the desired width
        .toBuffer();

      await fs.writeFile(target, resizedImage);
      // console.log(`Icon fetched and saved successfully: ${target}`);
    } else {
      // console.error(`Failed to fetch icon from ${url}. Status: ${response.status}`);
    }
  } catch (error) {
    console.error(`An error occurred while fetching icon from ${url}:`, error);
  }
};

const parseAssetParallel = async (urls) => {
  const requests = urls.map((url) => fetchFile(url, path.join("../orgs", `${parse(url).hostname}.json`)));
  await Promise.all(requests);
};

const fetchIconParallel = async (urls) => {
  const requests = urls.map((url) => fetchIcon(url, path.join("../icons", `${parse(url).hostname}_${path.parse(url).name}${path.parse(url).ext}`)));
  await Promise.all(requests);
};

/*
  Sanitizes the given `stellar.toml` metadata to conform to the expected structure.
    
  .. note:: These sanitizing rituals are created by trial and error when `gson`
      whines about parsing the JSON file. 
*/
const sanitizeAssets = (asset_toml) => {
  if (asset_toml["DOCUMENTATION"]) {
    const docs = asset_toml["DOCUMENTATION"];
    if (Array.isArray(docs))
      asset_toml["DOCUMENTATION"] = docs[0];
  }

  if (asset_toml["CURRENCIES"]) {
    const currencies = asset_toml["CURRENCIES"];
    if (Array.isArray(currencies)) {
      currencies.forEach(c => {
        const addresses = c["collateral_addresses"];
        if (addresses && !Array.isArray(addresses)) {
          c["collateral_addresses"] = [addresses];
        }

        const sigs = c["collateral_address_signatures"];
        if (sigs && !Array.isArray(sigs)) {
          c["collateral_address_signatures"] = [sigs];
        }

        const fixedNumber = c["fixed_number"];
        if (fixedNumber && fixedNumber.toString().includes(",")) {
          c["fixed_number"] = fixedNumber.toString().replace(",", "");
        }
      });
    }
    asset_toml["CURRENCIES"] = currencies;
  }

  return asset_toml;
};

const parseFile = async (target) => {
  let result = null;
  try {
    await fs.access(target);
    const data = await fs.readFile(target, 'utf8');
    result = toml.parse(data);
    result = sanitizeAssets(result);
  } catch (error) {
    if (error.code !== 'ENOENT') {
      console.error(`An error occurred while accessing file ${target}:`, error);
    }
  }
  return result;
};

/*
  Returns a hash of all the assets available in the asset list. This
  is the hash of the `;`-separated string of ordered, unique `ACCOUNTS`
  in the `stellar.toml` files retrieved from remote servers.
*/
const getAssetHash = (assets) => {
  const accounts = [];
  assets.forEach(s => {
    if (s['ACCOUNTS'] != undefined)
      accounts.push(s['ACCOUNTS']);
    else
      accounts.push([]);
  });

  accounts.sort();
  const m = crypto.createHash("sha256");
  m.update(accounts.join(";"), "utf-8");

  return b58.encode(m.digest());
};

const run = async () => {
  const unique = await getAssetList(false);

  if (unique !== null) {
    if (totalAssets < unique.length) {
      totalAssets = unique.length;

      await ensureDirectoryExists("../orgs");
      await ensureDirectoryExists("../icons");

      await parseAssetParallel(unique);

      const assets = [];
      const files = await fs.readdir("../orgs");
      for (const file of files) {
        const target = path.join("../orgs", file);
        const st = await parseFile(target);
        if (st !== null) {
          assets.push(st);
        }
      }

      const exported = {
        "updated": Date.now(),
        "assets": assets,
        "hash": getAssetHash(assets)
      };

      const short = {
        "updated": exported["updated"],
        "hash": exported["hash"]
      };
      // console.log(short);

      await fs.writeFile("../digital-world-hash.json", JSON.stringify(exported));
      await fs.writeFile("../digital-world-orgs.json", JSON.stringify(exported));

      const icons = new Set();
      assets.forEach(asset => {
        if (asset["CURRENCIES"]) {
          asset["CURRENCIES"].forEach(currency => {
            if (currency["code"] && currency["issuer"] !== "" && !currency["issuer"].includes("$") && currency["image"]) {
              icons.add(currency["image"]);
            }
          });
        }
      });

      assets.forEach(asset => {
        if (asset["DOCUMENTATION"]["ORG_LOGO"]) {
          icons.add(asset["DOCUMENTATION"]["ORG_LOGO"]);
        }
      });

      await fetchIconParallel(Array.from(icons));

      const unwound = new Map();
      const clean_assets = [];
      assets.forEach(asset => {
        if (asset["CURRENCIES"]) {
          asset["CURRENCIES"].forEach(currency => {
            if (currency["issuer"] !== "" && !currency["issuer"].includes("$") && currency["code"]) {
              const pair = [[currency['code'], currency['issuer']]];
              unwound.set(pair, asset);
              if (!clean_assets.includes(asset)) {
                clean_assets.push(asset);
              }
            }
          });
        }
      });

      const caps = new Map();
      for (let [code, issuer] of tqdm(Array.from(unwound.keys()))) {
        const r = await server.assets().forCode(code).forIssuer(issuer).limit(10).call();
        const pair = [code, issuer];
        caps.set(pair, r);
      }

      const combined = [];
      for (const [key, r] of caps) {
        if (r.records.length > 0) {
          combined.push({
            "code": key[0][0],
            "issuer": key[0][1],
            "amount": r.records[0]["amount"],
            "accounts": r.records[0]["num_accounts"]
          });
        }
      }

      combined.sort((a, b) => b.accounts - a.accounts);

      await fs.writeFile("../digital-world-assets.json", JSON.stringify(combined));
    } else {
      console.log("No new assets have been created since last execution. Skipping.");
    }
  }

  const time = new Date().toLocaleString('en-US', { timeZone: 'America/Denver' });
  console.log(`Completed at ${time} Mountain Time\n`);

  isRunning = false;
};

// Run every 60 seconds until stream is fixed
setInterval(run, 60000);

// const txHandler = function (txResponse) {
//   console.log(txResponse);

//   if (isRunning) {
//     console.log("Script is running. Waiting 60 seconds before trying again");
//     setTimeout(txHandler, 60000);
//   } else {
//     isRunning = true;
//     console.log("Script is not running. Continuing to run() function");

//     // Waiting 5 seconds before running script to allow asset to get setup completely
//     setTimeout(run, 5000);
//   }
// };

/*
  The wallet GAQ3ZTU4X64QB4TRSXVQ54RI4D5DAJJLA3TQFPAVZY54K56UDO372KXZ is where payments 
  go to create new assets on the Digital World.
*/
// const es = streamServer.payments()
//   .forAccount('GAQ3ZTU4X64QB4TRSXVQ54RI4D5DAJJLA3TQFPAVZY54K56UDO372KXZ')
//   .cursor(lastCursor)
//   .stream({
//     onmessage: txHandler
//   });

// process.on('SIGINT', function() {
//   console.log("\nCaught interrupt signal. Closing stream channel and exiting");
//   es();
//   process.exit();
// });
