# Digital World Stream

## Instructions
Make sure you are using node >= 16.0.0

If you have nvm, you can run ```nvm use``` to get the correct version of node.

Then run ```yarn install```

To start the program run ```yarn start```